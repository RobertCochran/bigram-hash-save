#!/usr/bin/env python3

# bigram-hash-save - sample save format for Joshua Brockschmidt's bigram_quoter
# Copyright (C) 2015 Robert Cochran
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Note to those who look :
# This program does *not* create new random bigrams!
# It merely arranges the data into a format of my suggestion,
# in a RFC type format.

import sys
import re
import pprint

bigram_words = {"!start!" : {}}

if len(sys.argv) == 1:
    print("Need to supply data file for analysis")
    sys.exit(-1)

# Initialize words array with dummy element for zip processing
words = [None]

with open(sys.argv[1], "r") as words_file:
    for line in words_file:
        # Strip words of newlines
        line_words = [re.sub("\n", "", x) for x in line.split(" ")]

        for i, word in enumerate(line_words):
            # Check if the word ends a sentence and is not lone punctuation
            if word not in ('.', '!', '?') and word[-1] in ('.', '!', '?'):
                # Insert ending punctuation as its own 'word'
                line_words.insert(i + 1, word[-1])
                # Remove the punctuation from the original word
                line_words[i] = word[:-1]

        words.extend(line_words)

for current_word, previous_word in zip(words[1:], words):
    if current_word not in bigram_words:
        bigram_words[current_word] = {}

    # First word in the sentence
    if previous_word in (None, '.', '!', '?'):
        # Set previous 'word' to sentence start marker
        previous_word = "!start!"

    if current_word not in bigram_words[previous_word]:
        bigram_words[previous_word][current_word] = 1;
    else:
        bigram_words[previous_word][current_word] += 1;


print("Bigram words: ")
pprint.pprint(bigram_words)
